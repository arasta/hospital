﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using BL;
using Domain;

namespace WebAppServer.Controllers
{
    public class DiseasesSymptomsController : Controller
    {
        private readonly ApplicationDbContext _context;

        public DiseasesSymptomsController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: DiseasesSymptoms
        public async Task<IActionResult> Index()
        {
            var applicationDbContext = _context.DiseasesSymptoms.Include(d => d.Disease).Include(d => d.Symptom);
            return View(await applicationDbContext.ToListAsync());
        }

        // GET: DiseasesSymptoms/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var diseasesSymptom = await _context.DiseasesSymptoms
                .Include(d => d.Disease)
                .Include(d => d.Symptom)
                .FirstOrDefaultAsync(m => m.DiseasesSymptomId == id);
            if (diseasesSymptom == null)
            {
                return NotFound();
            }

            return View(diseasesSymptom);
        }

        // GET: DiseasesSymptoms/Create
        public IActionResult Create()
        {
            ViewData["DiseaseId"] = new SelectList(_context.Diseases, "DiseaseId", "Name");
            ViewData["SymptomId"] = new SelectList(_context.Symptoms, "SymptomId", "Name");
            return View();
        }

        // POST: DiseasesSymptoms/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("DiseasesSymptomId,SymptomId,DiseaseId")] DiseasesSymptom diseasesSymptom)
        {
            if (ModelState.IsValid)
            {
                _context.Add(diseasesSymptom);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["DiseaseId"] = new SelectList(_context.Diseases, "DiseaseId", "Name", diseasesSymptom.DiseaseId);
            ViewData["SymptomId"] = new SelectList(_context.Symptoms, "SymptomId", "Name", diseasesSymptom.SymptomId);
            return View(diseasesSymptom);
        }

        // GET: DiseasesSymptoms/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var diseasesSymptom = await _context.DiseasesSymptoms.FindAsync(id);
            if (diseasesSymptom == null)
            {
                return NotFound();
            }
            ViewData["DiseaseId"] = new SelectList(_context.Diseases, "DiseaseId", "Name", diseasesSymptom.DiseaseId);
            ViewData["SymptomId"] = new SelectList(_context.Symptoms, "SymptomId", "Name", diseasesSymptom.SymptomId);
            return View(diseasesSymptom);
        }

        // POST: DiseasesSymptoms/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("DiseasesSymptomId,SymptomId,DiseaseId")] DiseasesSymptom diseasesSymptom)
        {
            if (id != diseasesSymptom.DiseasesSymptomId)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(diseasesSymptom);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!DiseasesSymptomExists(diseasesSymptom.DiseasesSymptomId))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["DiseaseId"] = new SelectList(_context.Diseases, "DiseaseId", "Name", diseasesSymptom.DiseaseId);
            ViewData["SymptomId"] = new SelectList(_context.Symptoms, "SymptomId", "Name", diseasesSymptom.SymptomId);
            return View(diseasesSymptom);
        }

        // GET: DiseasesSymptoms/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var diseasesSymptom = await _context.DiseasesSymptoms
                .Include(d => d.Disease)
                .Include(d => d.Symptom)
                .FirstOrDefaultAsync(m => m.DiseasesSymptomId == id);
            if (diseasesSymptom == null)
            {
                return NotFound();
            }

            return View(diseasesSymptom);
        }

        // POST: DiseasesSymptoms/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var diseasesSymptom = await _context.DiseasesSymptoms.FindAsync(id);
            _context.DiseasesSymptoms.Remove(diseasesSymptom);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool DiseasesSymptomExists(int id)
        {
            return _context.DiseasesSymptoms.Any(e => e.DiseasesSymptomId == id);
        }
    }
}
