﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using BL;
using BL.DTOs;
using BL.Interfaces.Services;
using Domain;

namespace WebAppServer.Controllers.api
{
    [Route("api/diseases")]
    [ApiController]
    public class DiseasesController : ControllerBase
    {
        private readonly IDiseaseService _diseaseService;

        public DiseasesController(IDiseaseService diseaseService, ISymptomService symptomService)
        {
            _diseaseService = diseaseService;
        }

        // GET: api/Diseases/TopThree
        [HttpGet("TopThree")]
        public async Task<List<DiseaseDTO>> GetTopThreeDiseases()
        {
            return await _diseaseService.GetTopThreeAsync();
        }

        // POST: api/Diseases/FindBySymptoms
        [HttpPost("FindBySymptoms")]
        public async Task<List<DiseaseDTO>> GetDiseasesBySymptoms([FromBody]List<SymptomDTO> symptoms)
        {
            return await _diseaseService.GetBySymptoms(symptoms);
        }

        // POST: api/Diseases/FindBySymptoms
        [HttpPost("RedoDatabase")]
        public async Task<List<string>> CrateNewDatabase([FromBody]List<string> dbLines)
        {
            return await _diseaseService.RedoDatabase(dbLines);
        }
    }
}