﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using BL.DTOs;
using BL.Interfaces.Services;

namespace WebAppServer.Controllers.api
{
    [Route("api/symptoms")]
    [ApiController]
    public class SymptomsController : ControllerBase
    {
        private readonly ISymptomService _symptomService;

        public SymptomsController(ISymptomService symptomService)
        {
            _symptomService = symptomService;
        }

        // GET: api/Symptoms
        [HttpGet]
        public async Task<List<SymptomDTO>> GetAllSymptomsAsync()
        {
            return await _symptomService.GetAllSymptomsAsync();
        }

        // GET: api/Symptoms/count
        [HttpGet("count")]
        public async Task<int> GetUniqueSymptomsCountAsync()
        {
            return await _symptomService.GetAllUniqueSymptomsCountAsync();
        }

        // GET: api/Symptoms/TopThree
        [HttpGet("TopThree")]
        public async Task<List<SymptomDTO>> GetTopThreeSymptoms()
        {
            return await _symptomService.GetTopThreeAsync();
        }
    }
}