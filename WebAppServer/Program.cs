﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using BL;
using Domain;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;

namespace WebAppServer
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var host = CreateWebHostBuilder(args).Build();

            using (var scope = host.Services.CreateScope())
            {
                var services = scope.ServiceProvider;
                try
                {
                    var context = services.GetRequiredService<ApplicationDbContext>();

                    InitializeDbAsync(context).Wait();
                }
                catch (Exception ex)
                {
                    var logger = services.GetRequiredService<ILogger<Program>>();
                    logger.LogError(ex, "An error ocurred while seeding initial application data!");
                }
            }

            host.Run();
        }

        private static async Task InitializeDbAsync(ApplicationDbContext context)
        {

            if (context.Diseases.Any()) return;
            var csvLines = File.ReadAllLines("Diseases.csv");


            foreach (var csvLine in csvLines)
            {
                // TODO: Read all lines to memory
                string[] list = csvLine.Split(',');
                var disease = new Disease() {Name = list[0] };
                await context.Diseases.AddAsync(disease);

                for (int i = 1; i < list.Length; i++)
                {
                    var symptom = await context.Symptoms.FirstOrDefaultAsync(s => s.Name == list[i]);
                    if (symptom == null)
                    {
                        symptom = new Symptom() { Name = list[i] };
                        await context.Symptoms.AddAsync(symptom);
                    }

                    await context.DiseasesSymptoms.AddAsync(new DiseasesSymptom()
                    {
                        Disease = disease,
                        Symptom = symptom
                    });
                    
                }

                await context.SaveChangesAsync();
                Console.WriteLine(csvLine);
            }
        }

        public static IWebHostBuilder CreateWebHostBuilder(string[] args) =>
            WebHost.CreateDefaultBuilder(args)
                .UseStartup<Startup>();
    }
}
