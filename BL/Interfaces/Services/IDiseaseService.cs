﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using BL.DTOs;
using Domain;

namespace BL.Interfaces.Services
{
    public interface IDiseaseService
    {
        Task<List<DiseaseDTO>> GetTopThreeAsync();
        Task<List<DiseaseDTO>> GetBySymptoms(List<SymptomDTO> symptoms);
        Task<List<string>> RedoDatabase(List<string> dbLines);
    }
}
