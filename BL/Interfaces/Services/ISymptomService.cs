﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using BL.DTOs;

namespace BL.Interfaces.Services
{
    public interface ISymptomService
    {
        Task<int> GetAllUniqueSymptomsCountAsync();
        Task<List<SymptomDTO>> GetTopThreeAsync();
        Task<List<SymptomDTO>> GetAllSymptomsAsync();
    }
}
