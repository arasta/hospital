﻿using System;
using System.Collections.Generic;
using System.Text;
using BL.DTOs;
using Domain;

namespace BL.Interfaces.Factories
{
    public interface IDiseaseFactory
    {
        DiseaseDTO Transform(Disease disease);
        DiseaseDTO TransformDetailed(Disease disease);
    }
}
