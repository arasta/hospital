﻿using System;
using System.Collections.Generic;
using System.Text;
using BL.DTOs;
using Domain;

namespace BL.Interfaces.Factories
{
    public interface ISymptomFactory
    {
        SymptomDTO Transform(Symptom symptom);
    }
}
