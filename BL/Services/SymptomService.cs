﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BL.DTOs;
using BL.Interfaces.Factories;
using BL.Interfaces.Services;
using Domain;
using Microsoft.EntityFrameworkCore;

namespace BL.Services
{
    public class SymptomService : ISymptomService
    {
        private readonly ApplicationDbContext _context;
        private readonly ISymptomFactory _symptomFactory;

        public SymptomService(ApplicationDbContext context, ISymptomFactory symptomFactory)
        {
            _context = context;
            _symptomFactory = symptomFactory;
        }

        public async Task<int> GetAllUniqueSymptomsCountAsync()
        {
            return await _context.Symptoms.Distinct().CountAsync();
        }

        public async Task<List<SymptomDTO>> GetTopThreeAsync()
        {
            var query = _context.Symptoms
                .OrderByDescending(x => x.SymptomsDiseases.Count)
                .ThenBy(x => x.Name)
                .Take(3);

            return await query.Select(x => _symptomFactory.Transform(x)).ToListAsync();
        }

        public async Task<List<SymptomDTO>> GetAllSymptomsAsync()
        {
            return await _context.Symptoms.Select(x => _symptomFactory.Transform(x)).ToListAsync();
        }
    }
}
