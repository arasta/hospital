﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using BL.DTOs;
using BL.Interfaces.Factories;
using BL.Interfaces.Services;
using Domain;
using Microsoft.EntityFrameworkCore;

namespace BL.Services
{
    public class DiseaseService : IDiseaseService
    {
        private readonly ApplicationDbContext _context;
        private readonly IDiseaseFactory _diseaseFactory;

        public DiseaseService(ApplicationDbContext context, IDiseaseFactory diseaseFactory)
        {
            _context = context;
            _diseaseFactory = diseaseFactory;
        }

        public async Task<List<DiseaseDTO>> GetTopThreeAsync()
        {
            var query = _context.Diseases
                .OrderByDescending(x => x.DiseasesSymptoms.Count)
                .ThenBy(x => x.Name)
                .Take(3);

            return await query.Select(x => _diseaseFactory.Transform(x)).ToListAsync();
        }

        public async Task<List<DiseaseDTO>> GetBySymptoms(List<SymptomDTO> symptoms)
        {
            var query = _context.Diseases.Include(x => x.DiseasesSymptoms).ThenInclude(x => x.Symptom).AsQueryable();
            foreach (var symptom in symptoms)
            {
                query = query.Where(x => x.DiseasesSymptoms.Any(y => y.SymptomId == symptom.SymptomId));
            }

            return await query.Select(x => _diseaseFactory.TransformDetailed(x)).ToListAsync();
        }

        public async Task<List<string>> RedoDatabase(List<string> dbLines)
        {
            _context.DiseasesSymptoms.RemoveRange(_context.DiseasesSymptoms);
            _context.Diseases.RemoveRange(_context.Diseases);
            _context.Symptoms.RemoveRange(_context.Symptoms);
            await _context.SaveChangesAsync();

            foreach (var dbLine in dbLines)
            {
                string[] splittedLine = dbLine.Split(',');
                var disease = new Disease() { Name = splittedLine[0] };
                await _context.Diseases.AddAsync(disease);
                for (int i = 1; i < splittedLine.Length; i++)
                {
                    var symptom = await _context.Symptoms.FirstOrDefaultAsync(s => s.Name == splittedLine[i]);
                    if (symptom == null)
                    {
                        symptom = new Symptom() { Name = splittedLine[i] };
                        await _context.Symptoms.AddAsync(symptom);
                    }

                    await _context.DiseasesSymptoms.AddAsync(new DiseasesSymptom()
                    {
                        Disease = disease,
                        Symptom = symptom
                    });
                }
                await _context.SaveChangesAsync();
            }
            return dbLines;
        }
    }
}
