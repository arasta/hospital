﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BL.DTOs;
using BL.Interfaces.Factories;
using Domain;

namespace BL.Factories
{
    public class DiseaseFactory : IDiseaseFactory
    {
        private readonly ISymptomFactory _symptomFactory;

        public DiseaseFactory(ISymptomFactory symptomFactory)
        {
            _symptomFactory = symptomFactory;
        }

        public DiseaseDTO Transform(Disease disease)
        {
            if (disease == null) return null;
            return new DiseaseDTO()
            {
                DiseaseId = disease.DiseaseId,
                Name = disease.Name
            };
        }

        public DiseaseDTO TransformDetailed(Disease disease)
        {
            if (disease == null) return null;
            return new DiseaseDTO()
            {
                DiseaseId = disease.DiseaseId,
                Name = disease.Name,
                Symptoms = disease.DiseasesSymptoms?.Select(x => _symptomFactory.Transform(x.Symptom)).ToList()
            };
        }
    }
}
