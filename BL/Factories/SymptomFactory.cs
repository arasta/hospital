﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BL.DTOs;
using BL.Interfaces.Factories;
using Domain;

namespace BL.Factories
{
    public class SymptomFactory : ISymptomFactory
    {

        public SymptomDTO Transform(Symptom symptom)
        {
            if (symptom == null) return null;
            return new SymptomDTO()
            {
                SymptomId = symptom.SymptomId,
                Name = symptom.Name
            };
        }


    }
}
