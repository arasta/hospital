﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace BL.DTOs
{
    public class DiseaseDTO
    {
        public int DiseaseId { get; set; }

        [Required]
        [MaxLength(200)]
        public string Name { get; set; }

        public List<SymptomDTO> Symptoms { get; set; }
    }
}
