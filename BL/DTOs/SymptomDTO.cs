﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace BL.DTOs
{
    public class SymptomDTO
    {
        public int SymptomId { get; set; }
        [Required]
        [MaxLength(200)]
        public string Name { get; set; }

        public List<DiseaseDTO> Diseases { get; set; }
    }
}
