﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebAppClient.Models;

namespace WebAppClient.Services.Interfaces
{
    public interface IDiseaseService
    {
        Task<List<Disease>> GetTopDiseasesAsync();
        Task<List<Disease>> GetDiseasesBySymptomsAsync(List<Symptom> symptoms);
        Task<List<string>> RedoDatabase(List<string> lines);
    }
}
