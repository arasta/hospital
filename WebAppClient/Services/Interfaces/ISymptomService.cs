﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebAppClient.Models;

namespace WebAppClient.Services.Interfaces
{
    public interface ISymptomService
    {
        Task<int> GetUniqueSymptomNumberAsync();
        Task<List<Symptom>> GetTopSymptomsAsync();
        Task<List<Symptom>> GetAllSymptomsAsync();
    }
}
