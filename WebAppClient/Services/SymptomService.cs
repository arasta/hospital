﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using WebAppClient.Models;
using WebAppClient.Services.Interfaces;

namespace WebAppClient.Services
{
    public class SymptomService : BaseService, ISymptomService
    {
        private readonly string _url;
        public SymptomService(IConfiguration configuration) : base(configuration)
        {
            _url = configuration["Api:Symptoms"];
        }


        public async Task<int> GetUniqueSymptomNumberAsync() => await GetAsync<int>($"{_url}/count");
        public async Task<List<Symptom>> GetTopSymptomsAsync() => await GetAsync<List<Symptom>>($"{_url}/TopThree");
        public async Task<List<Symptom>> GetAllSymptomsAsync() => await GetAsync<List<Symptom>>(_url);
    }
}
