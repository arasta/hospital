﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using WebAppClient.Models;
using WebAppClient.Services.Interfaces;

namespace WebAppClient.Services
{
    public class DiseaseService : BaseService, IDiseaseService
    {
        private readonly string _url;
        public DiseaseService(IConfiguration configuration) : base(configuration)
        {
            _url = configuration["Api:Diseases"];
        }

        public async Task<List<Disease>> GetTopDiseasesAsync() => await GetAsync<List<Disease>>($"{_url}/TopThree");
        public async Task<List<Disease>> GetDiseasesBySymptomsAsync(List<Symptom> symptoms) => await PostAsync<List<Disease>>($"{_url}/FindBySymptoms", symptoms);
        public async Task<List<string>> RedoDatabase(List<string> lines) => await PostAsync<List<string>>($"{_url}/RedoDatabase", lines);
    }
}
