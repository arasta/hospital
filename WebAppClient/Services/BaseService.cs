﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.AspNetCore.WebUtilities;
using Microsoft.Extensions.Configuration;

namespace WebAppClient.Services
{
    public class BaseService
    {
        protected readonly HttpClient Client;

        public BaseService(IConfiguration configuration)
        {
            Client = new HttpClient { BaseAddress = new Uri(configuration["Api:BaseUri"]) };
        }

        protected async Task<T> GetAsync<T>(string url)
        {
            var resp = await Client.GetAsync(url);
            resp.EnsureSuccessStatusCode();
            return await resp.Content.ReadAsAsync<T>();
        }

        protected async Task<T> PostAsync<T>(string url, Object obj)
        {
            var response = await Client.PostAsJsonAsync(url, obj);
            response.EnsureSuccessStatusCode();
            return await response.Content.ReadAsAsync<T>();
        }
    }
}
