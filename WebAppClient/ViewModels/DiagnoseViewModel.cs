﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebAppClient.Models;

namespace WebAppClient.ViewModels
{
    public class DiagnoseViewModel
    {
        public List<Symptom> AllSymptoms { get; set; }
        public List<Disease> DiseasesBySymptoms { get; set; }
    }
}
