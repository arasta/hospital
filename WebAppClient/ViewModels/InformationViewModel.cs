﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebAppClient.Models;

namespace WebAppClient.ViewModels
{
    public class InformationViewModel
    {
        public int SymptomCount { get; set; }
        public List<Symptom> TopSymptoms { get; set; }
        public List<Disease> TopDiseases { get; set; }
    }
}
