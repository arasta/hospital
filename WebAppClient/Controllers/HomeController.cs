﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using WebAppClient.Models;
using WebAppClient.Services.Interfaces;

namespace WebAppClient.Controllers
{
    public class HomeController : Controller
    {
        private readonly IDiseaseService _diseaseService;

        public HomeController(IDiseaseService diseaseService)
        {
            _diseaseService = diseaseService;
        }

        // GET: Home/Index
        public IActionResult Index()
        {
            return View();
        }

        // GET: Home/Upload
        public ActionResult Upload()
        {
            return View();
        }

        // POST: Home/Upload
        [HttpPost]
        public async Task<IActionResult> Upload(IFormFile file)
        {
            var filePath = Path.GetTempFileName();

            using (var stream = new FileStream(filePath, FileMode.Create))
            {
                await file.CopyToAsync(stream);
            }

            var lines = await System.IO.File.ReadAllLinesAsync(filePath);
            var list = new List<string>(lines);
            await _diseaseService.RedoDatabase(list);

            return RedirectToAction("Index", "Information");
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
