﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using WebAppClient.Models;
using WebAppClient.Services.Interfaces;
using WebAppClient.ViewModels;

namespace WebAppClient.Controllers
{
    public class DiagnoseController : Controller
    {
        private readonly ISymptomService _symptomService;
        private readonly IDiseaseService _diseaseService;

        public DiagnoseController(ISymptomService symptomService, IDiseaseService diseaseService)
        {
            _symptomService = symptomService;
            _diseaseService = diseaseService;
        }

        // GET: Diagnose
        public async Task<ActionResult> Index()
        {
            var vm = new DiagnoseViewModel();
            vm.AllSymptoms = await _symptomService.GetAllSymptomsAsync();
            return View(vm);
        }

        // POST: Diagnose/Diseases
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Diseases(DiagnoseViewModel vm)
        {
            List<Symptom> symptoms = vm.AllSymptoms.Where(x => x.IsSelected).ToList();
            vm.DiseasesBySymptoms = await _diseaseService.GetDiseasesBySymptomsAsync(symptoms);
            return View(vm);
        }
    }
}