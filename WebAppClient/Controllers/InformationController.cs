﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using WebAppClient.Services.Interfaces;
using WebAppClient.ViewModels;

namespace WebAppClient.Controllers
{
    public class InformationController : Controller
    {
        private readonly ISymptomService _symptomService;
        private readonly IDiseaseService _diseaseService;

        public InformationController(ISymptomService symptomService, IDiseaseService diseaseService)
        {
            _symptomService = symptomService;
            _diseaseService = diseaseService;
        }

        // GET: Information
        public async Task<ActionResult> Index()
        {
            var vm = new InformationViewModel();
            vm.SymptomCount = await _symptomService.GetUniqueSymptomNumberAsync();
            vm.TopSymptoms = await _symptomService.GetTopSymptomsAsync();
            vm.TopDiseases = await _diseaseService.GetTopDiseasesAsync();
            return View(vm);
        }
    }
}