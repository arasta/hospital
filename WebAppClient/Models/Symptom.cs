﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace WebAppClient.Models
{
    public class Symptom
    {
        public int SymptomId { get; set; }
        [Required]
        [MaxLength(200)]
        public string Name { get; set; }

        public bool IsSelected { get; set; } = false;

        public List<Disease> SymptomsDiseases { get; set; }
    }
}
