﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace WebAppClient.Models
{
    public class Disease
    {
        public int DiseaseId { get; set; }

        [Required]
        [MaxLength(200)]
        public string Name { get; set; }

        public List<Symptom> DiseasesSymptoms { get; set; }
    }
}
