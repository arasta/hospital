﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Domain
{
    public class Disease
    {
        public int DiseaseId { get; set; }

        [Required]
        [MaxLength(200)]
        public string Name { get; set; }

        public List<DiseasesSymptom> DiseasesSymptoms { get; set; }
    }
}
