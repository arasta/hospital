﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Domain
{
    public class Symptom
    {
        public int SymptomId { get; set; }
        [Required]
        [MaxLength(200)]
        public string Name { get; set; }

        public List<DiseasesSymptom> SymptomsDiseases { get; set; }
    }
}
