﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Domain
{
    public class DiseasesSymptom
    {
        public int DiseasesSymptomId { get; set; }

        public int SymptomId { get; set; }
        public Symptom Symptom { get; set; }

        public int DiseaseId { get; set; }
        public Disease Disease { get; set; }
    }
}
